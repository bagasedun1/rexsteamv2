<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $guarded =['id'];
    
    public function chart()
    {
        return $this->hasMany(Chart::class);
    }
    

    // public function cekmultipledata($query, array $filters)
    // {
    //     # code...
    // }
}
