<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $guarded =['id'];

    public function scopeFilter($query, array $filters)
    {
        //search header
        // dd($filters);
        $query->when($filters['searchAll'] ?? false,   function($query, $search){
            return $query->where('game_name', 'like', '%'.$search.'%');
        });
        // search manage game
        $query->when($filters['search'] ?? false,   function($query, $search){
            return $query->where('game_name', 'like', '%'.$search.'%');
        });
        $query->when(request('filter') ?? false,   function($query, $filter){
            return $query->whereIn('game_category', $filter);
        });
    }

    public function scopeDownloadVideo($query, $id)
    {
        
        $query->when($id ?? false,   function($query, $id){
            return $query=Storage::download('file.jpg');
        });
    }

    // public function chart()
    // {
    //     return $this->belongsTo(Chart::class);
    // }
    // public function transaction()
    // {
    //     return $this->belongsTo(Transaction::class);
    // }
}
