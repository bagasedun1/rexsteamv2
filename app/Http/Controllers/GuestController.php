<?php

namespace App\Http\Controllers;

use App\Models\Guest;
use App\Models\User;
use App\Models\Game;
use App\Models\Transaction;
use App\Models\Chart;
use Illuminate\Http\Request;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class GuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(request('searchAll'));
        return view('guest/front_page', ['games' => Game::latest()->filter(request(['searchAll']))->get()]);
    }

    public function formRegister()
    {
        return view('form/register');
    }

    public function formLogin()
    {
        return view('form/login');
    }

    public function create(Request $request)
    {
        $validatedData = $request->validate([
            'username' => 'required|min:6|max:191|unique:users',
            'fullname' => 'required|min:3|max:191',
            'password' => ['required', Password::min(8)->letters()->numbers()],
            'confirmPassword' => ['required', Password::min(8)->letters()->numbers()],
            'role' => 'required',
        ]);       
        $password = $request->password;
        $confirmPassword = $request->confirmPassword;
        if($password != $confirmPassword){
            return redirect()->back()->with('error', 'Please check your password!');
        }
        $validatedData['password']=bcrypt($validatedData['password']);
        User::create($validatedData);
        return redirect('login')->with('success', 'Register Complete');;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function show(Guest $guest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function edit(Guest $guest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Guest $guest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Guest  $guest
     * @return \Illuminate\Http\Response
     */
    public function destroy(Guest $guest)
    {
        //
    }

    public function checkAge(Request $request, $id) 
    {
        //make loop date
        $i='';
        $time = Carbon::now();
        $timeNow = $time->format('Y');
        $dataGame = Game::where('id', $id)->first();
        if($dataGame->game_age != 'adult'){
            $detailGame = Game::where('id', $id)
                                    ->first();
            $transaction = Chart::where('game_id', $detailGame->id)->first();
            return view('UtilityPage/detail_game', ['game' => $detailGame, 'transaction' => $transaction]);
        }
        return view('UtilityPage/check_age', ['game'=>$dataGame, 'i'=>$i,'time'=>$timeNow]);
    }
    public function detailGame(Request $request, $id) 
    {
        //check if date is true
        $detailGame = Game::where('id', $id)
                        ->first();
        if(request(['date'])){
            $day = request('date')[0];
            $month = request('date')[1];
            $year = request('date')[2];
            $old_date = $day.'-'.$month.'-'.$year;
            $formatDate = new DateTime($old_date);
            $userBirth = $formatDate->format('Y-m-d');
            // calculating age
            $date = $userBirth;
            $date = new DateTime($date); 
            $time = new DateTime();
            $diff = $date->diff($time);
            $age = $diff->y;
            if($age>=18){
                $detailGame = Game::where('id', $id)
                                    ->first();
                $transaction = Chart::where('game_id', $detailGame->id)->first();
                // dd($transaction);
                return view('UtilityPage/detail_game', ['game' => $detailGame, 'transaction' => $transaction]);
            }else{
                return view('guest/front_page', ['games' => Game::all()])->with('error', 'This category is intended for persons 18 years of age and older. Please DO NOT ENTER if you are under the age of 18.');
            }
        }

        return view('UtilityPage/detail_game', ['game' => $detailGame]);
    }

    public function cobaReceipt()
    {
        return view('shopping/transaction_receipt');
    }

    public function memberProfile()
    {

        return view('member/member_profile', [ 'user' => User::where('id', auth()->user()->id)->first()]);
    }

    public function updateMemberProfile(Request $request)
    {
        if($request->username != auth()->user()->username){
            $validatedData = $request->validate([
                'username' => 'required|min:6|max:191|unique:users',
                'fullname' => 'required|min:3|max:191',
                'image' => 'image|file|mimes:jpg|max:100',
                'level' => ''
            ]);
        }
        $validatedData = $request->validate([
            'username' => 'required|min:6|max:191',
            'fullname' => 'required|min:3|max:191',
            'image' => 'image|file|mimes:jpg|max:100',
            'level' => ''
        ]);
        
        // dd($request->all());
        if($request->current_password){
            if(Hash::check($request->current_password, auth()->user()->password)){
                $rules = $request->validate([
                    'password' => ['required', Password::min(8)->letters()->numbers()],
                    'confirm_password' => ['required', Password::min(8)->letters()->numbers()]
                ]);
                $password = $request->password;
                $confirmPassword = $request->confirm_password;
                if($password != $confirmPassword){
                    return redirect()->back()->with('error', 'Please check your password!');
                }

                if($request->image){
                    if($request->old_image){
                        Storage::delete($request->old_image);
                    }
                    //Begin::upload IMAGE
                    $originalName = $validatedData['image']->getClientOriginalName();
                    $timeNow = Carbon::now();
                    $formatTime = $timeNow->format('YmdHis');
                    $uniqImageName = $formatTime.$originalName;
                    $validatedData['image'] = $request->file('image')->storeAs('user-covers',$uniqImageName);
                    //End::upload IMAGE
                }else{
                    $validatedData['image'] = $request->old_image;
                }
                $newPassword = auth()->user()->update([
                    'password'=> bcrypt($request->password)
                ]);
                if($newPassword){
                    User::where('id', auth()->user()->id)
                    ->update($validatedData);
                    return redirect()->back()->with('success', 'Your Data Has been udpated!');
                }else{
                    return redirect()->back()->with('error', 'Something wrong!');
                }
                
                
            }
            throw validationException::withMessages([
                'current_password' => 'Yuor current password does not match with our record'
            ]);
        }

        if($request->image){
            if($request->old_image){
                Storage::delete($request->old_image);
            }
            //Begin::upload IMAGE
            $originalName = $validatedData['image']->getClientOriginalName();
            $timeNow = Carbon::now();
            $formatTime = $timeNow->format('YmdHis');
            $uniqImageName = $formatTime.$originalName;
            $validatedData['image'] = $request->file('image')->storeAs('user-covers',$uniqImageName);
            //End::upload IMAGE

        }else{
            $validatedData['image'] = $request->old_image;
        }

        User::where('id', auth()->user()->id)
                    ->update($validatedData);
        return redirect()->back()->with('success', 'Your Data Has been udpated!');
    }

    public function memberFriend()
    {
        return view('member/member_friend');
    }

    
}
