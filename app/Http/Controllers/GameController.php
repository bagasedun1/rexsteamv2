<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function show(Game $game)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function edit(Game $game, $id)
    {
        $game = Game::find($id);
        return view('Admin/update_game', ['game' =>$game]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Game $game, $id)
    {
        // dd($id);
        
        $validatedData = $request->validate([
            "game_name" => 'required|max:255',
            "game_description_short" => 'required|max:500',
            "game_description_long" => 'required|max:2000',
            "game_category" => 'required|max:50',  
            "game_developer" => 'required|max:50', 
            "game_publisher" => 'required|max:50', 
            "game_price" => 'required|numeric', 
            "image" => 'image|file|mimes:jpg|max:100',
            "video" => 'mimetypes:video/webm|max:100000',
            "game_age" => '',
        ]);
        
        if($request->image){
            if($request->oldImage){
                Storage::delete($request->oldImage);
            }
            //Begin::upload IMAGE
            $originalName = $validatedData['image']->getClientOriginalName();
            $timeNow = Carbon::now();
            $formatTime = $timeNow->format('YmdHis');
            $uniqImageName = $formatTime.$originalName;
            $validatedData['image'] = $request->file('image')->storeAs('game-covers',$uniqImageName);
            //End::upload IMAGE
        }else{
            $validatedData['image'] = $request->oldImage;
        }

        if($request->video){
            if($request->oldVideo){
                Storage::delete($request->oldVideo);
            }
            //Begin::upload Video
            $originalNameVideo = $validatedData['video']->getClientOriginalName();
            $timeNowVideo = Carbon::now();
            $formatTimeVideo = $timeNowVideo->format('YmdHis');
            $uniqVideoName = $formatTimeVideo.$originalNameVideo;
            $validatedData['video'] = $request->file('video')->storeAs('game-trailers',$uniqVideoName);
        //     //Begin::upload IMAGE
        }else{
            $validatedData['video'] = $request->oldVideo;
        }
        $updateGame = Game::where('id', $id)
                        ->update($validatedData);
            // dd($newGame);

        return redirect('/manage_game')->with('success', 'Game has been updated!!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Game  $game
     * @return \Illuminate\Http\Response
     */
    public function destroy(Game $game, $id)
    {
        $dataGame = Game::find($id);
        if($dataGame->image){
            Storage::delete($dataGame->image);
        }
        if($dataGame->video){
            Storage::delete($dataGame->video);
        }
        $dataGame->delete();
        return redirect('/manage_game')->with('success', 'Data has been deleted!!!');
    }
}
