<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\Game;
use App\Models\User;
use App\Models\Chart;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function chart()
    {
        // dd('masuk');
            $dataTransactions = Chart::where('user_id', auth()->user()->id)
                                        ->where('status', '=', 'not owned')
                                        ->get();
            $totalPrice = '';
            if($dataTransactions !=null){
                foreach($dataTransactions as $dataTransaction){
                    $price[] = $dataTransaction->game->game_price;
                    $totalPrice = array_sum($price);
                }
            }
            if($totalPrice == null){
                $totalPrice = null;
            }
        return view( 'shopping/shopping_chart' , ['transactions' => $dataTransactions, 'totalPrice' => $totalPrice]);
    }

    public function shoppingChart(Request $request, $id)
    {

        if($id){
            $newChart = Chart::create([
                'user_id' => auth()->user()->id,
                'game_id' => $id
            ]);
            
            return redirect('/shopping_chart')->with('success', 'Success add to chart!!!');
        }
        $dataTransactions = Chart::where('user_id', auth()->user()->id)
                                        ->where('status', '=', 'not owned')
                                        ->get();
        $totalPrice = '';
        if($dataTransactions !=null){
            foreach($dataTransactions as $dataTransaction){
                $price[] = $dataTransaction->game->game_price;
                $totalPrice = array_sum($price);
            }
        }
        if($totalPrice == null){
            $totalPrice = null;
        }
        return view('shopping/shopping_chart', ['transactions' => $dataTransactions, 'totalPrice' => $totalPrice]);
    }

    public function shoppingInformation(Request $request)
    {
        $dataTransactions = Chart::where('user_id', auth()->user()->id)
                                    ->where('status', '=', 'not owned')
                                    ->get();
        $totalPrice = '';
        if($dataTransactions !=null){
            foreach($dataTransactions as $dataTransaction){
                $price[] = $dataTransaction->game->game_price;
                $totalPrice = array_sum($price);
            }
        }
        if($totalPrice == null){
            $totalPrice = null;
        }
        return view('shopping/transaction_information', ['totalPrice' => $totalPrice]);
    }

    public function updateShoppingInformation(Request $request)
    {
        // dd($request->total_price);
        $validatedData = $request->validate([
            'card_name' => 'required|min:6|max:255',
            'card_number'=> 'required',
            'expired_date_month'=> 'required|numeric|min:1|max:12',
            'expired_date_year'=> 'required|numeric|min:2021|max:2050',
            'cvc_cvv'=> 'required|numeric|min:100|max:999',
            'card_country'=> 'required',
            'zip_postal_code'=> 'required|numeric',
            'total_price'=> '',
        ]);
        $newTransactionTransaction = Transaction::create($validatedData);
        if($newTransactionTransaction){
            $data = Chart::where('user_id', auth()->user()->id)
                        ->where('status', '=', 'not owned')
                        ->get();
            if($data){
                // dd($newTransactionTransaction->id);
                $dataChart = '';
                $dataChart = [
                    'transaction_id' => $newTransactionTransaction->id,
                    'total_price' => $validatedData['total_price'],
                    'status' => 'owned'
                ];
                // dd($dataChart);
                $updateChart = Chart::where('user_id', auth()->user()->id)
                                    ->where('status', '=', 'not owned')
                                    ->update($dataChart);
                $dataUser = User::where('id', auth()->user()->id)->first();
                $level = $dataUser->level;
                $levelUp = [
                    'level' => $level+1
                ];
                $updateLevel = User::where('id', auth()->user()->id)->update($levelUp);
                return redirect('/shopping/transaction_receipt')->with('success', 'Game has been purchased!!!');
            }
        }
        return redirect('/shopping/transaction_receipt')->with('success', 'Game has been purchased!!!');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show(Transaction $transaction)
    {
        $transactions = Transaction::all();
        $owneds = Chart::where('user_id', auth()->user()->id)
                        ->where('status', 'owned')
                        ->get();
        return view('/shopping/transaction_receipt', ['transactions' => $transactions,'owneds' => $owneds]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Transaction $transaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaction $transaction, $id)
    {
        $transaction = Chart::find($id);
        $transaction->delete();
        return redirect('/shopping_chart')->with('success', 'Data has been deleted!!!');
    }

    public function memberHistory()
    {
        $transactions = Transaction::all();
        $owneds = Chart::where('user_id', auth()->user()->id)
                        ->where('status', 'owned')
                        ->get();
        return view('member/member_transaction_history', ['transactions' => $transactions,'owneds' => $owneds]);
    }
}
