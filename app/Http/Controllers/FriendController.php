<?php

namespace App\Http\Controllers;

use App\Models\Friend;
use App\Models\Guest;
use App\Models\User;
use App\Models\Game;
use App\Models\Transaction;
use App\Models\Chart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Redirect;

class FriendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $friends = Friend::where('friend_id', auth()->user()->id)
                    ->where('status', 'friend')
                    ->get();
        $dataPendingFriends = Friend::where('user_id',auth()->user()->id)
                                ->where('status', 'pending')
                                ->get();
        $pendingFriends = [];
        foreach($dataPendingFriends as $dataPendingFriend){
            $pendingFriends[] = User::where('id', $dataPendingFriend->friend_id)->first();
        }

        $incomingFriends = Friend::where('friend_id',auth()->user()->id)
                                ->where('status', 'pending')
                                ->get();
        return view('member/member_friend', ['friends' => $friends, 'pendingFriends' => $pendingFriends, 'incomingFriends' => $incomingFriends]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Friend  $friend
     * @return \Illuminate\Http\Response
     */
    public function show(Friend $friend)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Friend  $friend
     * @return \Illuminate\Http\Response
     */
    public function edit(Friend $friend)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Friend  $friend
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Friend $friend, $friendId)
    {
        // dd($friendId);
        // dd(auth()->user()->id);
        $data = [
            'status' => 'friend'
        ];
        $addFriendRelation = [
            'user_id' => auth()->user()->id,
            'friend_id' => $friendId,
            'status' => 'friend'
        ];
        $update = Friend::where('user_id', '=', $friendId)
                        ->where('friend_id', '=', auth()->user()->id)
                        ->update($data);
        $addBack = Friend::create($addFriendRelation);
        return redirect('/member_friend')->with('success', 'friend has been added!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Friend  $friend
     * @return \Illuminate\Http\Response
     */
    public function cancel(Friend $friend, $pendingFriendId)
    {
        $cancel = Friend::where('user_id', '=', auth()->user()->id)
                        ->where('friend_id', '=', $pendingFriendId)
                        ->where('status', 'pending')
                        ->first();
        $cancel->delete();
        return redirect()->back()->with('success', 'Friend request has been canceled!');
    }

    public function reject(Friend $friend, $incomingFriendId)
    {
        // dd($incomingFriendId);
        $reject = Friend::find($incomingFriendId);
        $reject->delete();
        return redirect()->back()->with('success', 'Friend request has been rejected!');
    }

    public function addFriend(Request $request)
    {
        // dd(request(['search_user']));
        $username = request(['search_user']);
        if($username){
            $dataUser = User::where('username', '=', $username)->first();
            // dd($dataUser);
            if($dataUser != null){
                $checkFriend = Friend::where('user_id', auth()->user()->id)
                                    ->where('friend_id', $dataUser->id)
                                    ->first();
                if($checkFriend == null){
                    $data = [
                        'user_id' => auth()->user()->id,
                        'friend_id' => $dataUser->id,
                        'status' => 'pending'
                    ];
                    $reqestFriend = Friend::create($data);
                    return redirect()->back()->with('success', 'request has been successfully sent!');
                }else{
                    return redirect()->back()->with('error', 'Your friend request is not acceped yet!');
                }
            }else{
                return redirect()->back()->with('error', 'username does not exist!');
            }
        }
    }
}
