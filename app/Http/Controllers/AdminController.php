<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Game;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\Password;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // iki sing tak gawe
        // dd(request(['search']));
        return view('Admin/admin_page', ['games' => Game::latest()->filter(request(['search', 'searchAll']))->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->all());
        $validatedData =$request->validate([
            "game_name" => 'required|max:255',
            "game_description_short" => 'required|max:500',
            "game_description_long" => 'required|max:2000',
            "game_category" => 'required|max:50',  
            "game_developer" => 'required|max:50', 
            "game_publisher" => 'required|max:50', 
            "game_price" => 'required|numeric', 
            "image" => 'image|file|mimes:jpg|max:100',
            "video" => 'mimetypes:video/webm|max:100000',
            "game_age" => ''
        ]);
        // dd($validatedData);
        //Begin::upload IMAGE
        $originalName = $validatedData['image']->getClientOriginalName();
        $timeNow = Carbon::now();
        $formatTime = $timeNow->format('YmdHis');
        $uniqImageName = $formatTime.$originalName;
        $validatedData['image'] = $request->file('image')->storeAs('game-covers',$uniqImageName);
        //End::upload IMAGE

        //Begin::upload Video
        $originalNameVideo = $validatedData['video']->getClientOriginalName();
        $timeNowVideo = Carbon::now();
        $formatTimeVideo = $timeNowVideo->format('YmdHis');
        $uniqVideoName = $formatTime.$originalNameVideo;
        $validatedData['video'] = $request->file('video')->storeAs('game-trailers',$uniqVideoName);
        //Begin::upload IMAGE

        Game::create($validatedData);
        return redirect('/manage_game')->with('success', 'Berhasil Menambahkan Data Baru!!!');
    }

    public function uiCreate()
    {
        return view('Admin/create_game');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //filter on model game. Passing value from "request(['filter]) to model game"
        return view('Admin/manage_game', ['games' => Game::latest()->filter(request(['search', 'filter']))->get()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        //
    }

    public function profileAdmin()
    {
        return view('Admin/admin_profile'); 
    }

    public function updateProfileAdmin(Request $request)
    {
        if($request->username != auth()->user()->username){
            $validatedData = $request->validate([
                'username' => 'required|min:6|max:191|unique:users',
                'fullname' => 'required|min:3|max:191',
                'image' => 'image|file|mimes:jpg|max:100'
            ]);
        }
        $validatedData = $request->validate([
            'username' => 'required|min:6|max:191',
            'fullname' => 'required|min:3|max:191',
            'image' => 'image|file|mimes:jpg|max:100'
        ]);

        if($request->current_password){
            if(Hash::check($request->current_password, auth()->user()->password)){
                $rules = $request->validate([
                    'password' => ['required', Password::min(8)->letters()->numbers()],
                    'confirm_password' => ['required', Password::min(8)->letters()->numbers()]
                ]);
                $password = $request->password;
                $confirmPassword = $request->confirm_password;
                if($password != $confirmPassword){
                    return redirect()->back()->with('error', 'Please check your password!');
                }

                if($request->image){
                    if($request->old_image){
                        Storage::delete($request->old_image);
                    }
                    //Begin::upload IMAGE
                    $originalName = $validatedData['image']->getClientOriginalName();
                    $timeNow = Carbon::now();
                    $formatTime = $timeNow->format('YmdHis');
                    $uniqImageName = $formatTime.$originalName;
                    $validatedData['image'] = $request->file('image')->storeAs('user-covers',$uniqImageName);
                    //End::upload IMAGE
                }else{
                    $validatedData['image'] = $request->old_image;
                }
                $newPassword = auth()->user()->update([
                    'password'=> bcrypt($request->password)
                ]);
                if($newPassword){
                    User::where('id', auth()->user()->id)
                    ->update($validatedData);
                    return redirect()->back()->with('success', 'Your Data Has been udpated!');
                }else{
                    return redirect()->back()->with('error', 'Something wrong!');
                }
                
                
            }
            throw validationException::withMessages([
                'current_password' => 'Yuor current password does not match with our record'
            ]);
        }

        if($request->image){
            if($request->old_image){
                Storage::delete($request->old_image);
            }
            //Begin::upload IMAGE
            $originalName = $validatedData['image']->getClientOriginalName();
            $timeNow = Carbon::now();
            $formatTime = $timeNow->format('YmdHis');
            $uniqImageName = $formatTime.$originalName;
            $validatedData['image'] = $request->file('image')->storeAs('user-covers',$uniqImageName);
            //End::upload IMAGE

        }else{
            $validatedData['image'] = $request->old_image;
        }

        User::where('id', auth()->user()->id)
                    ->update($validatedData);
        return redirect()->back()->with('success', 'Your Data Has been udpated!');
    }
}
