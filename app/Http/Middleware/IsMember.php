<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->guest() || auth()->user()->role == 'admin'){
            return redirect('/admin');
        }else if(auth()->guest() || auth()->user()->role !== 'member'){
            return redirect('/login');
        }
        return $next($request);
    }
}
