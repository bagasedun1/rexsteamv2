<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Game;
use App\Models\Chart;
use App\Models\Transaction;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'username' => 'bagasedun99',
            'fullname' => 'bagasedun99',
            'password' => bcrypt('bagasedun1'),
            'role'     => 'member'
        ]);
        User::create([
            'username' => 'bagasedun88',
            'fullname' => 'bagasedun88',
            'password' => bcrypt('bagasedun1'),
            'role'     => 'member'
        ]);
        User::create([
            'username' => 'bagasedun1',
            'fullname' => 'bagasedun1',
            'password' => bcrypt('bagasedun1'),
            'role'     => 'admin'
        ]);
        Chart::create([
            'user_id' => '1',
            'game_id' => '1'
            // 'transaction_id' => '1'
        ]);
        Transaction::create([
            'id' => '1'
        ]);

        Game::create([
            'game_name' => 'truck binjai',
            'game_description_short'=> 'dadadadad',
            'game_description_long'=> 'addadadadd',
            'game_category'=> 'honnor',
            'game_price'=> '1000000',
            'game_developer'=> 'orang binjai',
            'game_publisher'=> 'binjai project',
            'image'=> 'game-covers/20211121093354EuroTruckSimulator2.jpg',
            'video'=> 'game-tralers/20211121093354EuroTruckSimulator2Trailer.webm',
            'game_age'=> 'adult'
        ]);
    }
}
