<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->string('card_name')->nullable();
            $table->string('card_number')->nullable();
            $table->integer('expired_date_month')->nullable();
            $table->integer('expired_date_year')->nullable();
            $table->integer('cvc_cvv')->nullable();
            $table->string('card_country')->nullable();
            $table->integer('zip_postal_code')->nullable();
            $table->integer('total_price')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
