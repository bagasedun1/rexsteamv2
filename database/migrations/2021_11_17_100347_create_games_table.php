<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->string('game_name');
            $table->text('game_description_short', 500);
            $table->text('game_description_long', 2000);
            $table->string('game_category');
            $table->integer('game_price');
            $table->string('game_developer');
            $table->string('game_publisher');
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->string('game_age')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
