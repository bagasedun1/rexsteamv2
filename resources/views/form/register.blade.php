<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
    <link href="{{ asset('style/form-style.css') }}" rel="stylesheet">
</head>
<body>

    <main>

        <div class="container">

            <img src="{{asset('image/ForzaHorizon4.jpg')}}" alt="Logo">

            <div class="container-login">

                <div class="login-body">

                    <h3>Register Page</h3>
                    <!-- alert error -->
                    @if(session()->has('error'))
                        <div class="alert alert-danger" style="color: red;font-weight:bold">
                            {{ session()->get('error') }}
                        </div>
                    @endif

                    <form method="post" action="register" class="forms">
                        @csrf    
                    <div class="mb-3 A">
                        <label for="username" class="form-label">Username</label>
                        <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="username" required>
                        @error('username')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="fullName" class="form-label">Full Name</label>
                        <input type="text" class="form-control @error('fullname') is-invalid @enderror" id="fullname" name="fullname" required>
                        @error('fullname')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" required>
                        @error('password')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    
                    <div class="mb-3">
                        <label for="confirmPassword" class="form-label">Confirm Password</label>
                        <input type="password" class="form-control" id="confirmPassword" name="confirmPassword" required>
                    </div>

                    <label for="role">Role</label> <br>
                    <select id="role" name="role" class="menu-drop">
                        <option value="member">Member</option>
                        <option value="admin">Admin</option>
                    </select> 
                    <br>

                    <button type="submit" class="btn btn-primary">Sign Up</button>

                    <a href="{{ url('login')}}">Already have an account ?</a>

                    </form>
                </div>

            </div>

        </div>
        
    </main>
    
</body>

</html>