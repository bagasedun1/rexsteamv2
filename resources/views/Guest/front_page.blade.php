<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
</head>
<body>
    
    <main>

        <header>
            <!-- Component Header -->
            @include('component.header')
        </header>

        <div class="ruler"></div>

        <section class="mysection">
            
            <div class="mysection-container">
                
                <h1 class="sp-text">Top Games</h1>
                <div class="container-space">
                    <div class="container-space-content">
                        @if(count($games) < 1)
                        <p>No data!!</p>
                        @else
                        @foreach ($games as $game)
                        <div class="container-space-content-detail">
                            <a href="{{ url('check_age/'.$game->id)}}"><img src="{{ asset('storage/'.$game->image) }}" alt=""></a>
                            <h1>{{ $game->game_name }}</h1>
                            <h2>{{ $game->game_category}}</h2>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
            </div>

        </section>

        <footer>
            <!-- Component Footer -->
            @include('component.footer')
        </footer>
        
    </main>

</body>

</html>