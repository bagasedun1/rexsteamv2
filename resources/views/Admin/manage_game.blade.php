<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
</head>
<body>

    <main>

        <header>
            <!-- Component Header -->
            @include('component.header_admin')
        </header>

        <section class="mysection">

            <div class="mysection-container">
                <h1 class="sp-text">Manage Games</h1>
                
                <div class="filter-box">

                    <div class="filter-box-field">
                        <h2>Filter By Games Name</h2>
                        <form action="/manage_game">
                            <div class="search-box">
                                <i class="fas fa-search icon"></i>
                                <input type="text" placeholder="Search.." name="search">
                            </div>

                            <h2>Filter By Category Games</h2>
                            <div class="filter-checkbox">
                                <input type="checkbox" id="idle" name="filter[]" value="idle">
                                <label for="idle">Idle</label>
                            </div>

                            <div class="filter-checkbox">
                                <input type="checkbox" id="horror" name="filter[]" value="horror">
                                <label for="horror">Horror</label>
                            </div>

                                <div class="filter-checkbox">
                                <input type="checkbox" id="adventure" name="filter[]" value="adventure">
                                <label for="adventure">Adventure</label>
                            </div>

                            <div class="filter-checkbox">
                                <input type="checkbox" id="action" name="filter[]" value="action">
                                <label for="action">Action</label>
                            </div>

                            <div class="filter-checkbox">
                                <input type="checkbox" id="sport" name="filter[]" value="sport">
                                <label for="sport">Sport</label>
                            </div>

                            <div class="filter-checkbox">
                                <input type="checkbox" id="strategy" name="filter[]" value="strategy">
                                <label for="strategy">Strategy</label>
                            </div>

                            <div class="filter-checkbox">
                                <input type="checkbox" id="rpg" name="filter[]" value="rpg">
                                <label for="rpg">RPG</label>
                            </div>

                            <div class="filter-checkbox filter">
                                <input type="checkbox" id="puzzle" name="filter[]" value="puzzle">
                                <label for="puzzle">Puzzle</label>
                            </div>

                            <div class="filter-checkbox">
                                <input type="checkbox" id="simulation" name="filter[]" value="simulation">
                                <label for="simulation">Simulation</label>
                            </div> <br>
                            <button type="submit">
                                Search
                            </button>
                        </form>
                        
                        <div class="container-space">
                            <div class="container-space-content">
                                @if(count($games) < 1)
                                <h2>There are no games content can be showed right now.</h2>
                                @else
                                @foreach ($games as $game)
                                <div class="container-space-content-detail">
                                    <a href=""><img src="{{ asset('storage/'.$game->image) }}" alt=""></a>
                                    <h1>{{ $game->game_name }}</h1>
                                    <h2>{{ $game->game_category}}</h2>
                                    <div class="tools">
                                        
                                        <div class="tools left">
                                            <button>
                                                <i class="fas fa-pencil-alt">
                                                    <a href="/manage_game/edit/{{ $game->id }}">Update</a> 
                                                </i>
                                            </button>
                                        </div>

                                        <div class="tools right">
                                            <form action="/manage_game/delete/{{ $game->id }}" method="post">
                                                @method('delete')
                                                @csrf
                                                
                                                    <button onclick="return confirm('Are You Sure ?')"><i class="far fa-trash-alt">Delete</i></button>
                                                
                                            </form>
                                        </div>

                                    </div>
                                </div>
                                @endforeach
                                @endif
                            </div>
                        </div>

                    </div>

                </div>

                <div class="create-new-game">
                    <a href="{{ url('manage_game/create_game')}}"><i class="fas fa-file-medical"></i></a>
                </div>
                
            </div>

        </section>

        <footer>
            <!-- Component Footer -->
            @include('component.footer')
        </footer>

    </main>

</body>
</html>