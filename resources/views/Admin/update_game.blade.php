<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
</head>
<body>

    <main>
    
        <header>
            <!-- Component Header -->
            @include('component.header_admin')
        </header>

        <section class="mysection">

            <div class="mysection-container">
                <h1 class="sp-text">Edit Game</h1>

                <div class="form-create-game">
                    <div class="main-form-create-game">
                        <form action="/manage_game/edit/{{ $game->id }}" method="post" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <label for="">Game Name</label><br>
                            <input type="text" name="game_name" value="{{ old('game_name', $game->game_name)}}">
                            @error('game_name')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>
                            

                            <label for="">Game Description</label> <br>
                            <textarea name="game_description_short" id="" cols="25" rows="3"  value="{{ old('game_description_short', $game->game_description_short)}}"><?php echo $game['game_description_short'] ?></textarea>
                            @error('game_description_short')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>
                            <p>Write a single santace about the game.</p>

                            <label for="">Game Long Description</label> <br>
                            <textarea name="game_description_long" id="" cols="25" rows="8" value="{{ old('game_description_long', $game->game_description_long)}}"><?php echo $game['game_description_long'] ?></textarea>
                            @error('game_description_long')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <p>Write a few santace about the game.</p>

                            <label for="">Game Category</label> <br>
                            <select id="game_category" name="game_category" class="menu-drop" value="{{ old('game_category', $game->game_category)}}">
                                <option value="{{$game->game_category}}">{{$game->game_category}}</option>
                                <option value="idle">idle</option>
                                <option value="honnor">honnor</option>
                                <option value="adventure">adventure</option>
                                <option value="action">action</option>
                                <option value="sport">sport</option>
                                <option value="strategy">strategy</option>
                                <option value="role playing">role playing</option>
                                <option value="puzzle">puzzle</option>
                                <option value="simulation">simulation</option>
                            </select> 
                            @error('game_category')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>

                            <label for="">Game Developer</label><br>
                            <input type="text" name="game_developer" class="menu-drop" value="{{ old('game_developer', $game->game_developer)}}">
                            @error('game_developer')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>

                            <label for="">Game Publisher</label><br>
                            <input type="text" name="game_publisher" class="menu-drop" value="{{ old('game_publisher', $game->game_publisher)}}">
                            @error('game_publisher')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>

                            <label for="">Game Price</label><br>
                            <input type="text" name="game_price" class="menu-drop" value="{{ old('game_price', $game->game_price)}}">
                            @error('game_price')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>

                            <label for="">Game Cover</label>
                            <br>
                            <div class="extra-zone">
                                <div class="drop-zone">
                                    <p></p>
                                    <span class="drop-zone__prompt"> 
                                    <i class="far fa-images"></i><br>    
                                    Drag and drop your file or click in this area. <br>
                                    JPG up to 100KB </span>
                                    <input type="hidden" name="oldImage" value="{{$game->image}}">
                                    <input type="file" name="image" class="drop-zone__input">
                                </div>
                                    @error('image')
                                    <div class="invalid-feedback center">
                                        <span class="drop-zone__prompt ">{{ $message }}</span>
                                    </div>
                                    @enderror
                            </div>


                            <label for="">Game Trailer</label><br>
                            <div class="extra-zone">
                                <div class="drop-zone">

                                    <span class="drop-zone__prompt"> 
                                    <i class="fas fa-video"></i><br>    
                                    Drag and drop your file or click in this area. <br>
                                    MKV up to 100MB </span>
                                    <input type="hidden" name="oldVideo" value="{{$game->video}}">
                                    <input type="file" name="video" class="drop-zone__input">

                                </div>
                            </div>


                            <div class="filter-checkbox">
                                @if($game->game_age)
                                <input type="hidden" name="game_age" value="">
                                <input type="checkbox" id="game_age" name="game_age" value="adult" checked>
                                <label for="game_age" >Only For Adult</label>
                                @else
                                <input type="hidden" name="game_age" value="">
                                <input type="checkbox" id="game_age" name="game_age" value="adult">
                                <label for="game_age" >Only For Adult</label>
                                @endif
                            </div>

                            <div class="action-execute">
                                <input type="submit" value="Cancel">
                                <input type="submit" value="Save">
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </section>

    <footer>
        <!-- Component Footer -->
        @include('component.footer')
    </footer>

    </main>
    
    <script src="{{ asset('js/script.js')}}"></script>
    
</body>

</html>