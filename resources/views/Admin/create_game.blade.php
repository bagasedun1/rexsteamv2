<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
</head>
<body>

    <main>
    
        <header>
            <!-- Component Header -->
            @include('component.header_admin')
        </header>

        <section class="mysection">

            <div class="mysection-container">
                <h1 class="sp-text">Create Games</h1>

                <div class="form-create-game">
                    <div class="main-form-create-game">
                        <form action="/manage_game/create_game/posts" method="post" enctype="multipart/form-data">
                            @csrf
                            <label for="">Game Name</label><br>
                            <input type="text" name="game_name" value="{{ old('game_name')}}">
                            @error('game_name')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>
                            

                            <label for="">Game Description</label> <br>
                            <textarea name="game_description_short" id="" cols="25" rows="3"  value="{{ old('game_description_short')}}"></textarea>
                            @error('game_description_short')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>
                            <p>Write a single santace about the game.</p>

                            <label for="">Game Long Description</label> <br>
                            <textarea name="game_description_long" id="" cols="25" rows="8" value="{{ old('game_description_long')}}"></textarea>
                            @error('game_description_long')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <p>Write a few santace about the game.</p>

                            <label for="">Game Category</label> <br>
                            <select id="game_category" name="game_category" class="menu-drop" value="{{ old('game_category')}}">
                                <option value="idle">Idle  </option>
                                <option value="honnor">Honnor</option>
                                <option value="adventure">Adventure</option>
                                <option value="action">Action</option>
                                <option value="sport">Sport</option>
                                <option value="strategy">Strategy</option>
                                <option value="rpg">Rpg</option>
                                <option value="puzzle">Puzzle</option>
                                <option value="simulation">Simulation</option>
                            </select> 
                            @error('game_category')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>

                            <label for="">Game Developer</label><br>
                            <input type="text" name="game_developer" class="menu-drop" value="{{ old('game_developer')}}">
                            @error('game_developer')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>

                            <label for="">Game Publisher</label><br>
                            <input type="text" name="game_publisher" class="menu-drop" value="{{ old('game_publisher')}}">
                            @error('game_publisher')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>

                            <label for="">Game Price</label><br>
                            <input type="text" name="game_price" class="menu-drop" value="{{ old('game_price')}}">
                            @error('game_price')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                            <br>

                            <label for="">Game Cover</label>
                            <br>
                            <div class="extra-zone">
                                <div class="drop-zone">
                                    <p></p>
                                    <span class="drop-zone__prompt"> 
                                    <i class="far fa-images"></i><br>    
                                    Drag and drop your file or click in this area. <br>
                                    JPG up to 100KB </span>
                                    <input type="file" name="image" class="drop-zone__input">
                                    
                                </div>
                                    @error('image')
                                    <div class="invalid-feedback center">
                                        <span class="drop-zone__prompt ">{{ $message }}</span>
                                    </div>
                                    @enderror
                            </div>


                            <label for="">Game Trailer</label><br>
                            <div class="extra-zone">
                                <div class="drop-zone">

                                    <span class="drop-zone__prompt"> 
                                    <i class="fas fa-video"></i><br>    
                                    Drag and drop your file or click in this area. <br>
                                    MKV up to 100MB </span>
                                    <input type="file" name="video" class="drop-zone__input">

                                </div>
                            </div>


                            <div class="filter-checkbox">
                                <input type="checkbox" id="game_age" name="game_age" value="adult">
                                <label for="game_age">Only For Adult</label>
                            </div>

                            <div class="action-execute">
                                <input type="submit" value="Cancel">
                                <input type="submit" value="Save">
                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </section>

        <footer>
            <!-- Component Footer -->
            @include('component.footer')
        </footer>

    </main>
    
    <script src="{{ asset('js/script.js')}}"></script>
    
</body>
</html>