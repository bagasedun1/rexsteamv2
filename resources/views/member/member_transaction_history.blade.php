<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
</head>

<body>

    <main>

        <header>
            <!-- Component Header -->
            @include('component.header')
        </header>

        <section class="mysection">

            <div class="profile-container">

                <div class="profile-field">
                    <div class="profile-zone left">
                    <ul class="first-ul" id="myDiv">
                        <li><a href="{{url('/member_profile')}}" class="nav-link"><i class="far fa-user"></i>&nbsp Profile</a></li>
                        <li><a href="{{url('/member_friend')}}" class="nav-link "><i class="far fa-heart"></i>&nbsp Friend</a></li>
                        <li><a href="{{url('/member_transaction_history')}}" class="nav-link show"><i class="fas fa-shopping-basket"></i>&nbsp Transaction History</a></li>
                    </ul>

                    </div>

                    <div class="profile-zone right">
                        <Span>Transaction History</Span>

                        @if(count($transactions))
                        @foreach($transactions as $transaction)
                        <div class="history-container">
                        
                            <div class="transaction-id">
                                <span>Transaction ID: {{ $transaction->id }}</span> <br>
                                <span>Purchased Date: {{ $transaction->updated_at}}</span>   
                            </div>

                            @foreach($owneds as $owned)
                            @if($transaction->id == $owned->transaction_id)
                            <div class="transaction-item">
                                <div class="transaction-item A">
                                    <a href=""><img src="{{asset('storage/'.$owned->game->image)}}" alt=""></a>
                                </div>
                            </div>
                            @endif
                            @endforeach

                            <div class="transaction-total-price">
                                <span>Total Price: <label for="">Rp {{ $transaction->total_price}}</label></span>
                            </div>
                        </div>
                        <hr>
                        @endforeach
                        @else
                        <div class="transaction-id">
                            <span>No Data Found</span> <br>  
                        </div>
                        @endif     
                    </div>                  

            </div>
        </section>

    <footer>
        <!-- Component Footer -->
        @include('component.footer')
    </footer>

    </main>
    
</body>
</html>