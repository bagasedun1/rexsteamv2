<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
</head>

<body>

    <main>

        <header>
            <!-- Component Header -->
            @include('component.header')
        </header>

        <section class="mysection">

            <div class="profile-container">

                <div class="profile-field">
                    <div class="profile-zone left">
                    <ul class="first-ul" id="myDiv">
                        <li><a href="{{url('/member_profile')}}" class="nav-link"><i class="far fa-user"></i>&nbsp Profile</a></li>
                        <li><a href="{{url('/member_friend')}}" class="nav-link show"><i class="far fa-heart"></i>&nbsp Friend</a></li>
                        <li><a href="{{url('/member_transaction_history')}}" class="nav-link"><i class="fas fa-shopping-basket"></i>&nbsp Transaction History</a></li>
                    </ul>

                    </div>

                    <div class="profile-zone right">
                        <Span>Friends</Span>
                        
                        <div class="add-friend">
                            <form action="/member_friend" method="post">
                                @csrf
                                <div class="add-friend-box">
                                    <label>Add Friend</label> <br>
                                    <input type="text" placeholder="Username" name="search_user">
                                    <button type="submit">Add</button>
                                </div>
                            </form>
                        </div>
                        
                        <div class="friend-list">
                            <label for="">Incoming Friend Request</label> <br>
                            @if(count($incomingFriends))
                            @foreach($incomingFriends as $incomingFriend)
                            <div class="friend-list-zone">
                                <div class="friend-list-zone A">
                                    <div class="friend-info">
                                    
                                        <div class="friend-info right">
                                            <img src="{{asset('storage/'.$incomingFriend->user->image)}}" alt="">
                                        </div>

                                        <div class="frined-info left">
                                            <p>{{$incomingFriend->user->username }}<span class="friend-lvl">{{ $incomingFriend->user->level}}</span> </p>
                                            <span>{{$incomingFriend->user->role}}</span>
                                        </div>

                                    </div>
                                    
                                    <div class="friend-action">
                                        <hr>
                                        <form action="/member_friend/update/{{$incomingFriend->user->id}}" method="post">
                                            @method('put')
                                            @csrf    
                                            <button type="submit">Accept</button>
                                        </form>
                                        <form action="/member_friend/reject/{{ $incomingFriend->id }}" method="post">
                                            @method('delete')
                                            @csrf
                                        <button onclick="return confirm('Are You Sure ?')">Reject</button>
                                        </form>
                                    </div>
                                    
                                </div>
                                @endforeach
                                @else
                                <span>There is no incoming friend request</span> <br>
                                @endif
                        </div>

                        <div class="friend-list">

                            <div class="friend-list-zone">
                                <label for="">Pending Friend Request</label> <br>
                                @if(count($pendingFriends))
                                @foreach($pendingFriends as $pendingFriend)
                                <div class="friend-list-zone A">
                                    <div class="friend-info">

                                        <div class="friend-info right">
                                            <img src="{{asset('storage/'. $pendingFriend->image)}}" alt="">
                                        </div>

                                        <div class="friend-info left">
                                            <p>{{ $pendingFriend->username }} <span class="friend-lvl">{{ $pendingFriend->level}}</span> </p>
                                            <span>{{ $pendingFriend->role}}</span>
                                        </div>

                                    </div>
                                    
                                    <div class="friend-action">
                                        <hr>
                                        <form action="/member_friend/cancel/{{$pendingFriend->id}}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button onclick="return confirm('Are You Sure ?')">Cancel</button>
                                        </form>
                                    </div>
                                </div>
                                @endforeach
                                @else
                                <span>There is no pending friend request</span> <br>
                                @endif
                            </div>

                            <div class="friend-list-zone">
                                <label>Your Friends</label> <br>
                                @if(count($friends))
                                @foreach($friends as $friend)
                                <div class="friend-list-zone A">
                                    <div class="friend-info">

                                        <div class="friend-info right">
                                            <img src="{{asset('storage/'.$friend->user->image)}}" alt="">
                                        </div>

                                        <div class="friend-info left">
                                            <p>{{ $friend->user->username}} <span class="friend-lvl">{{ $friend->user->level}}</span> </p>
                                            <span>{{ $friend->user->role}}</span>
                                        </div>

                                    </div>
                                    
                                </div>
                                @endforeach
                                @else
                                <span>There is no no friend</span>  <br>
                                @endif
                            </div>

                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </section>

    <footer>
        <!-- Component Footer -->
        @include('component.footer')
    </footer>

    </main>

</body>

</html>