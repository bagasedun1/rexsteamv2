<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
</head>
<body>

    <main>

        <header>
            <!-- Component Header -->
            @include('component.header')
        </header>

        <div class="nothing"></div>
        <div class="nothing"></div>

        <section class="mysection">

            <div class="profile-container">

                <div class="profile-field">
                    <div class="profile-zone left">
                    <ul class="first-ul" id="myDiv">
                        <li><a href="{{url('/member_profile')}}" class="nav-link show"><i class="far fa-user"></i>&nbsp Profile</a></li>
                        <li><a href="{{url('/member_friend')}}" class="nav-link"><i class="far fa-heart"></i>&nbsp Friend</a></li>
                        <li><a href="{{url('/member_transaction_history')}}" class="nav-link"><i class="fas fa-shopping-basket"></i>&nbsp Transaction History</a></li>
                    </ul>

                    </div>

                    <div class="profile-zone right">
                        <Span>admin1 Profile</Span>
                        <p>This information will be displayed publcly so be careful what you share.</p>
                        <form action="/member_profile/update" method="post" enctype="multipart/form-data">
                            @method('put')
                            @csrf
                            <div class="profile-picture">
                                <div class="drop-zone">
                                    <span class="drop-zone__prompt"> 
                                        Drag and drop your file or click in this area.    
                                    </span>
                                    <input type="hidden" name="old_image"  value="{{ auth()->user()->image }}">
                                    <input type="file" name="image" class="drop-zone__input">
                                </div>
                            </div>
    
                            <div class="profile-name">
                                <div class="profile-name left">
                                    <label for="username">Username</label> <br>
                                    <input type="text" name="username" id="username" value="{{ old('username', auth()->user()->username) }}">
                                        @error('username')
                                        <div class="invalid-feedback">
                                            <p class="text-danger">{{ $message }}</p>
                                        </div>
                                        @enderror
                                </div>
                                <div class="profile-name right">
                                    <label for="level">Level</label> <br>
                                    <input type="text" name="level" id="level" value="{{ old('username', auth()->user()->level) }}" readonly>
                                </div>
                            </div>
    
                            <div class="profile-fullname">
                                <label for="fullname">Fullname</label> <br>
                                <input type="text" name="fullname" id="fullname" value="{{ old('username', auth()->user()->fullname) }}">
                                    @error('fullname')
                                    <div class="invalid-feedback">
                                        <p class="text-danger">{{ $message }}</p>
                                    </div>
                                    @enderror
                            </div>
    
                            <div class="profile-password">
    
                                <label for="current_password">Current Password</label> <br>
                                <input type="password" name="current_password" id="current_password">
                                    @error('current_password')
                                    <div class="invalid-feedback">
                                        <p class="text-danger">{{ $message }}</p>
                                    </div>
                                    @enderror
                                <p>Fill out this field to check if you are authorized</p>
    
                                <label for="password">New Password</label> <br>
                                <input type="password" name="password" id="password"> 
                                    @error('password')
                                    <div class="invalid-feedback">
                                        <p class="text-danger">{{ $message }}</p>
                                    </div>
                                    @enderror
                                <p>Only if you want to change password</p>
    
                                <label for="confirm_password">Confirm New Password</label> <br>
                                <input type="password" name="confirm_password" id="confirm_password"> 
                                    @error('confirm_password')
                                    <div class="invalid-feedback">
                                        <p class="text-danger">{{ $message }}</p>
                                    </div>
                                    @enderror
                                <p>Only if you want to change password</p>
    
                            </div>
    
                            <div class="profil-update">
                                <button type="submit">Update Profile</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </section>

    <footer>
        <!-- Component Footer -->
        @include('component.footer')
    </footer>

    </main>

    <script src="{{ asset('js/script.js')}}"></script>
</body>
</html>