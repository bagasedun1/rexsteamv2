<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
    <link href="{{ asset('style/utility.css') }}" rel="stylesheet">
</head>
<body>

    <main>
        
        <header>
            <!-- Component Header -->
            @include('component.header')
        </header>

        <section class="mysection">
            
            <div class="detail-container">
                <h1>{{ $game->game_name }}</h1>
                <div class="detail-holder">
                    
                    <video class="detail-holder left" controls muted>
                        <source src="{{ asset('storage/'.$game->video) }}" alt=""  srcset="" type="video/WebM">
                    </video>

                    <div class="detail-holder right">
                        <div class="image-holder">
                            <img src="{{ asset('storage/'.$game->image) }}" alt="">
                        </div>

                        <div class="detail-info-game">
                            <div class="detail-game-title">
                                <span class="span-games-title">{{ $game->game_name }}</span>
                            </div>

                            <div class="detail-game-short-desc">
                                <span class="span-short-desc">
                                {{ $game->game_description_short }}
                                </span>
                            </div>

                            <div class="detail-game-dev-info">
                                <p class="span-dev-info">Genre: <label>{{ $game->game_category }}</label> </p>
                                <p class="span-dev-info">Relese Date: <label>{{ $game->created_at }}</label> </p> 
                                <p class="span-dev-info">Developer: <label>{{ $game->game_developer }}</label> </p> 
                                <p class="span-dev-info">Publisher: <label>{{ $game->game_publisher }}</label> </p> 
                            </div>
                        </div>
                    </div>
                
                </div>

            <!-- for Buy Stuff -->
            <div class="buy-stuff">
                <div class="buy-stuff-text">
                    <span>Buy {{ $game->game_name }}</span>
                </div>

                <div class="buy-stuff-btn">
                    <div class="all-stuff">
                        @if($transaction != true)
                        <span class="span-price-text">Rp {{ $game->game_price }}</span>
                        <form action="/shopping_chart/buy/{{ $game->id }}" method="post">
                            @csrf
                            <!-- <input type="hidden" name="game_id" value="{{ $game->id }}"> -->
                            <button type="submit" class="btn-buy-stuff"><i class="fas fa-shopping-cart"></i>&nbsp ADD TO CART</button>
                        </form>
                        @else
                        <span class="span-price-text">Rp {{ $game->game_price }}</span>
                        <span class="span-oncart"><a href="/shopping_chart">on chart</a></span>
                        @endif
                    </div>
                </div>
            </div>

            <!-- for long desc -->
            <div class="detail-long-desc">
                <h1>ABOUT THIS GAME</h1>
                <HR></HR>
                <p class="p-detail-long-desc">
                {{ $game->game_description_long }}
                </p>
            </div>

            </div>
        </section>

    <footer>
        <!-- Component Footer -->
        @include('component.footer')
    </footer>

    </main>


    
</body>
</html>