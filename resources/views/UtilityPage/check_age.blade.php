<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
    <link href="{{ asset('style/utility.css') }}" rel="stylesheet">
</head>
<body>

    <main>

        <header>
            <!-- Component Header -->
            @include('component.header')
        </header>

        <div class="ruler"></div>
        <div class="ruler"></div>
        <div class="ruler"></div>

        <section class="mysection">

            <div class="util-container">

                <div class="util-container-conten">
                <!-- <h1>asd</h1> -->
                    <div class="util-container-conten-imgholder">
                        <img src="{{ asset('storage/'.$game->image) }}" alt="">
                    </div>

                    <div class="util-container-content-warning">
                        <p class="warn">CONTENT IN THIS PRODUCT MAY NOT BE APPROPRIATE FOR ALL AGES,
                            OR MAY NOT BE APPROPRIATE FOR VIEWING AT WORK.
                        </p>
                    </div>

                    <div class="util-container-content-birthdate">
                        <p class="bdate">Please enter your birth date to continue:</p>

                        
                        <form action="/detail_game/{{ $game->id }}">
                            <div class="select-bdate">
                                
                                <div class="select-bdate A">
                                    <label for="">Date</label> <br>
                                    <select id="date" name="date[]" class="menu-drop">
                                    @for ($i = 1; $i <= 31; $i++)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor

                                    </select> 
                                </div>

                                <div class="select-bdate B">
                                    <label for="">Month</label> <br>
                                    <select id="month" name="date[]" class="menu-drop">
                                        <option value="1">January</option>
                                        <option value="2">February</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select> 
                                </div>

                                <div class="select-bdate C">
                                    <label for="">Year</label> <br>
                                    <select id="year" name="date[]" class="menu-drop">
                                        @for ($i = 1980; $i <= $time; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select> 
                                </div>
                                
                                <div class="btn-confirm">
                                    <button type="submit" class="btn btn-primary">View Page</button>
                                    <span class="like-button"><a href="{{ URL::previous() }}">Cancel</a></span>
                                </div>
                            </div>
                        </form>
                        

                    </div>
                </div>

            </div>

        </section>

    </main>

    <footer>
        <!-- Component Footer -->
        @include('component.footer')
    </footer>
    
</body>
</html>