<div class="header-container">
    <div class="header-container-field">
        <div class="header-container-field left">
            <div class="header-logo">
               <a href="{{ url('admin') }}"> <img src="{{asset('image/RexSteamLogo.png')}}" alt="Logo"></a>
            </div>
        </div>
        <div class="header-container-field right">
            <div class="header-menu">
                <ul>
                    <li><a href="{{ url('admin') }}">Home</a></li>
                    @auth
                    <li><a href="{{ url('manage_game')}}">Manage Game</a></li>

                    <li class="coba">
                        <div class="profile-picture">
                            <img onclick="see()" src="{{asset('image/RexSteamLogo.png')}}" alt="">
                        
                            <div id="myDropdown" class="dropdown-menu">
                                <a href="{{ url('/admin_profile') }}">Profile</a> <br>
                                <form action="/logout" method="post">
                                    @csrf
                                    <button type="submit">
                                        Sign Out
                                    </button>
                                </form>
                            </div>
                        </div>
                    </li>

                    <li class="coba">
                        <span >{{ auth()->user()->username }}</span>      
                    </li>
                    @else
                    <li class="coba"><a href="{{ url('login')}}">Login</a></li>
                    <li class="coba"><a href="{{ url('register')}}">Register</a></li>
                    @endauth
                    <li class="coba">
                        <div class="search-box">
                            <form action="/admin">
                                <button type="submit" class="btn-search">
                                <i class="fas fa-search"></i>
                                </button>
                                <input type="text" placeholder="Search.." name="search">
                                
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- alert success --> 
            @if(!empty($success))
                <div class="alert alert-success" id="modalAlert">
                    {{ $success }}
                </div>
            @elseif(session()->has('success'))
                <div class="alert alert-success" id="modalAlert">
                    {{ session()->get('success') }}
                </div>
            @endif
            <!-- END::Alert success -->
            <!-- alert error -->
            @if(!empty($error))
                <div class="alert alert-danger id="modalAlert"">
                    {{ $error }}
                </div>
            @elseif(session()->has('error'))
            <div class="alert alert-danger id="modalAlert"">
                {{ session()->get('error') }}
            </div>
            @endif
            <!-- END::Alert error -->
        </div>
    </div>
</div>

