<!DOCTYPE html>
<html lang="en">
<head>   
    <!-- Component Head -->
    @include('component.head')
    <link href="{{ asset('style/utility.css') }}" rel="stylesheet">
</head>
<body>

    <main>

        <header>
            <!-- Component Header -->
            @include('component.header')
        </header>

        <section class="mysection">
            
            <div class="shopping-cart-container">
                <h1>Shopping Cart</h1>
                <div class="shopping-cart-detail-container">

                    @if(count($transactions)>0)
                    @foreach($transactions as $transaction)
                    <div class="shopping-cart-detail">
                        <div class="shopping-cart-info">
                            <a href=""><img src="{{asset('storage/'.$transaction->game->image)}}" alt="" srcset=""></a>
                        </div>
                        <div class="shopping-cart-info-detail">
                            <span class="shopping-cart-info-name">
                                {{ $transaction->game->game_name }}
                            </span> 
                            <span class="shopping-cart-genre">
                                {{ $transaction->game->game_category}}
                            </span>
                            <p>
                                <i class="fas fa-tag tag">&nbsp</i>
                                Rp {{ $transaction->game->game_price }}
                            </p>
                        </div>
                        <div class="shopping-cart-delete">
                            <!-- &nbsp ojo dihapus -->
                            <form action="/shopping_chart/delete/{{ $transaction->id }}" method="post">
                                @method('delete')
                                @csrf
                                <button onclick="return confirm('Are You Sure ?')"><i class="far fa-trash-alt"></i>&nbsp Delete</button>
                            </form>
                        </div>          
                    </div>
                    @endforeach
                    @else
                    <p>no data!!!</p>
                    @endif

                    @if($totalPrice!=null)
                    <div class="shopping-total">
                        <label class="total-price">Total Price</label> <span>Rp {{ $totalPrice}}</span>
                        
                    </div>
                    @else
                    <div class="shopping-total">
                        <label class="total-price">Total Price</label> <span>Rp 0</span>
                        
                    </div>
                    @endif
                    <div class="shopping-cart-checkout">
                        <form action="/shopping_information">
                            <button type="submit"><i class="fas fa-truck"></i>&nbsp Checkout</button>
                        </form>
                    </div>
                </div>
            </div>

        </section>

    <footer>
        <!-- Component Footer -->
        @include('component.footer')
    </footer>

    </main>
    
</body>

</html>