<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Component Head -->
    @include('component.head')
    <link href="{{ asset('style/utility.css') }}" rel="stylesheet">
</head>
<body>

    <main >      

    <header>
        <!-- Component Header -->
        @include('component.header')
    </header>

    <div class="nothing"></div>

    <section class="mysection">

        <div class="transaction-information">

            <div class="transaction-information-container">
                
                <div class="transaction-information-form">

                <h1>Transaction Information</h1>
                <form action="/shopping_information/update" method="post">
                    @method('put')
                    @csrf
                    <div class="info-cc">
                        <label for="card_name">Card Name</label> <br>
                        <input type="text" name="card_name" id="card_name" placeholder="Card Name" required value="{{ old('card_name') }}"> <br>
                        @error('card_name')
                        <div class="invalid-feedback">
                            <p class="text-danger">{{ $message }}</p>
                        </div>
                        @enderror
                    </div>

                    <div class="info-cc">
                        <label for="card_number">Card Number</label> <br>
                        <input type="text" name="card_number" id="card_number" placeholder="0000 0000 0000 0000" class="cc-number-input" required value="{{ old('card_number') }}"> <br>
                        @error('card_number')
                        <div class="invalid-feedback">
                            <p class="text-danger">{{ $message }}</p>
                        </div>
                        @enderror
                        <label for="">Visa or Master Card</label> <br>
                    </div>

                    <div class="transaction-information-date">
                        
                        <div class="transaction-information-date A">
                            <label for="expired_date_month">Expired Date</label> <br>
                            <input type="text" class="date-info" name="expired_date_month" id="expired_date_month" placeholder="MM" required value="{{ old('expired_date_month') }}"> <br>
                            @error('expired_date_month')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                        </div>
                        
                        <div class="transaction-information-date B">
                            <input type="text" class="date-info" name="expired_date_year" id="expired_date_year" placeholder="YYYY" required value="{{ old('expired_date_year') }}"> <br>
                            @error('expired_date_year')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                        </div>
                        
                        <div class="transaction-information-date C">
                            <label for="cvc_cvv">CVC/CVV</label> <br>
                            <input type="text" class="date-info" name="cvc_cvv" id="cvc_cvv" placeholder="3 or 4 Digital Number" required value="{{ old('cvc_cvv') }}"> <br>
                            @error('cvc_cvv')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                        </div>
                        
                    </div>
                    
                    <div class="transaction-information-country">
                        
                        <div class="transaction-information-country A">
                            <label for="card_country">Country</label> <br>
                            <select id="card_country" name="card_country" class="country-info" value="{{ old('card_country')}}">
                                <option value="indonesia">Indonesia</option>
                                <option value="singapura">Singapura</option>
                                <option value="malaysia">Malaysia</option>
                                <option value="laos">Laos</option>
                                <option value="philipina">Philipina</option>
                                <option value="myanmar">Myanmar</option>
                                <option value="vietnam">Vietnam</option>
                                <option value="kamboja">Kamboja</option>
                                <option value="thailand">Thailand</option>
                            </select>  <br>
                        </div>
                        
                        <div class="transaction-information-country B">
                            <label for="zip_postal_code">ZIP</label> <br>
                            <input type="text" class="country-info" name="zip_postal_code" id="zip_postal_code" placeholder="ZIP" required value="{{ old('zip_postal_code') }}"> <br>
                            @error('zip_postal_code')
                            <div class="invalid-feedback">
                                <p class="text-danger">{{ $message }}</p>
                            </div>
                            @enderror
                        </div>

                    </div>
                    
                    @if($totalPrice!=null)
                    <div class="shopping-total">
                        <input type="hidden" name="total_price" value="{{ $totalPrice}}">
                        <label class="total-price">Total Price</label> <span>Rp {{ $totalPrice}}</span>
                    </div>
                    @else
                    <div class="shopping-total">
                        <label class="total-price">Total Price</label> <span>Rp 0</span>
                    </div>
                    @endif
                    
                    <div class="transaction-btn">
                        <button type="submit" class="btn-transaction"><i class="fas fa-truck"></i>&nbsp Checkout</button>   
                        <span class="like-button cart"><a href="{{ URL::previous() }}">Cancel</a></span>
                    </div>
                </form>
                
                </div>
            </div>
            
        </div>

    </section>


    <footer>
        <!-- Component Footer -->
        @include('component.footer')
    </footer>

    </main>

    <script>
        let ccNumberInput = document.querySelector('.cc-number-input'),
		ccNumberPattern = /^\d{0,16}$/g,
		ccNumberSeparator = " ",
		ccNumberInputOldValue,
		ccNumberInputOldCursor,
		
		ccExpiryInput = document.querySelector('.cc-expiry-input'),
		ccExpiryPattern = /^\d{0,4}$/g,
		ccExpirySeparator = "/",
		ccExpiryInputOldValue,
		ccExpiryInputOldCursor,
		
		ccCVCInput = document.querySelector('.cc-cvc-input'),
		ccCVCPattern = /^\d{0,3}$/g,
		
		mask = (value, limit, separator) => {
			var output = [];
			for (let i = 0; i < value.length; i++) {
				if ( i !== 0 && i % limit === 0) {
					output.push(separator);
				}
				
				output.push(value[i]);
			}
			
			return output.join("");
		},
		unmask = (value) => value.replace(/[^\d]/g, ''),
		checkSeparator = (position, interval) => Math.floor(position / (interval + 1)),
		ccNumberInputKeyDownHandler = (e) => {
			let el = e.target;
			ccNumberInputOldValue = el.value;
			ccNumberInputOldCursor = el.selectionEnd;
		},
		ccNumberInputInputHandler = (e) => {
			let el = e.target,
					newValue = unmask(el.value),
					newCursorPosition;
			
			if ( newValue.match(ccNumberPattern) ) {
				newValue = mask(newValue, 4, ccNumberSeparator);
				
				newCursorPosition = 
					ccNumberInputOldCursor - checkSeparator(ccNumberInputOldCursor, 4) + 
					checkSeparator(ccNumberInputOldCursor + (newValue.length - ccNumberInputOldValue.length), 4) + 
					(unmask(newValue).length - unmask(ccNumberInputOldValue).length);
				
				el.value = (newValue !== "") ? newValue : "";
			} else {
				el.value = ccNumberInputOldValue;
				newCursorPosition = ccNumberInputOldCursor;
			}
			
			el.setSelectionRange(newCursorPosition, newCursorPosition);
			
			highlightCC(el.value);
		},
		highlightCC = (ccValue) => {
			let ccCardType = '',
					ccCardTypePatterns = {
						amex: /^3/,
						visa: /^4/,
						mastercard: /^5/,
						disc: /^6/,
						
						genric: /(^1|^2|^7|^8|^9|^0)/,
					};
			
			for (const cardType in ccCardTypePatterns) {
				if ( ccCardTypePatterns[cardType].test(ccValue) ) {
					ccCardType = cardType;
					break;
				}
			}
			
			let activeCC = document.querySelector('.cc-types__img--active'),
					newActiveCC = document.querySelector(`.cc-types__img--${ccCardType}`);
			
			if (activeCC) activeCC.classList.remove('cc-types__img--active');
			if (newActiveCC) newActiveCC.classList.add('cc-types__img--active');
		},
		ccExpiryInputKeyDownHandler = (e) => {
			let el = e.target;
			ccExpiryInputOldValue = el.value;
			ccExpiryInputOldCursor = el.selectionEnd;
		},
		ccExpiryInputInputHandler = (e) => {
			let el = e.target,
					newValue = el.value;
			
			newValue = unmask(newValue);
			if ( newValue.match(ccExpiryPattern) ) {
				newValue = mask(newValue, 2, ccExpirySeparator);
				el.value = newValue;
			} else {
				el.value = ccExpiryInputOldValue;
			}
		};

        ccNumberInput.addEventListener('keydown', ccNumberInputKeyDownHandler);
        ccNumberInput.addEventListener('input', ccNumberInputInputHandler);
    </script>
    
    
</body>

</html>