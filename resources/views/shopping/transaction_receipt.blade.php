<!DOCTYPE html>
<html lang="en">
<head>   
    <!-- Component Head -->
    @include('component.head')
    <link href="{{ asset('style/utility.css') }}" rel="stylesheet">
</head>
<body>

    <main>

        <header>
            <!-- Component Header -->
            @include('component.header')
        </header>

        <section class="mysection">
            
            <div class="shopping-cart-container">
                <h1>Transaction Receipt</h1>
                <div class="shopping-cart-detail-container">
                    @if(count($transactions))
                    @foreach($transactions as $transaction)
                        <div class="shopping-cart-detail">
                            <p>Transaction ID: {{ $transaction->id }}</p>
                            <p>Purchased Date: {{ $transaction->updated_at}}</p>
                        @foreach($owneds as $owned)
                            @if($transaction->id == $owned->transaction_id)
                            <div class="shopping-cart-info">
                                <img src="{{asset('storage/'.$owned->game->image)}}" alt="" srcset="">
                            </div>
                            <!-- <div class="shopping-cart-info-detail">
                                <span class="shopping-cart-info-name">
                                    {{ $owned->game->game_name }}
                                </span> 
                                <p>
                                    <i class="fas fa-tag tag">&nbsp</i>
                                    Rp {{ $owned->game->game_price}}
                                </p>
                            </div>           -->
                            @endif
                            @endforeach
                        </div>
                        <div class="shopping-total">
                            <label class="total-price">Total Price</label> <span>Rp {{ $transaction->total_price }}</span>
                        </div>
                    @endforeach
                    @else
                    <p>no data!!!</p>
                    @endif

                </div>
            </div>

        </section>

    <footer>
        <!-- Component Footer -->
        @include('component.footer')
    </footer>

    </main>


    
</body>
</html>