<?php

use Illuminate\Support\Facades\Route;
use App\http\Controllers\GuestController;
use App\http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//admin:top
// profile admin
Route::put('/admin_profile/update', 'AdminController@updateProfileAdmin')->name('update_profile_admin')->middleware('admin');
Route::get('/admin_profile', 'AdminController@profileAdmin')->name('profile_admin')->middleware('admin');
Route::delete('/manage_game/delete/{id}', 'GameController@destroy')->name('destroy_game')->middleware('admin');
Route::put('/manage_game/edit/{id}', 'GameController@update')->name('update_game')->middleware('admin');
Route::get('/manage_game/edit/{id}', 'GameController@edit')->name('edit_game')->middleware('admin');
Route::post('/manage_game/create_game/posts', 'AdminController@create')->name('create_game')->middleware('admin');
Route::get('/manage_game/create_game', 'AdminController@uiCreate')->name('ui_create_game')->middleware('admin');
Route::get('/manage_game', 'AdminController@show')->name('manage_game')->middleware('admin');
Route::get('/admin', 'AdminController@index')->name('admin')->middleware('admin');
//admin:bot


Route::post('/logout', 'LoginController@logout')->name('logout');
Route::post('/login', 'LoginController@authenticate')->name('login');
Route::get('/login', 'GuestController@formLogin')->name('form_login')->middleware('guest');
Route::post('/register', 'GuestController@create')->name('create_account');
Route::get('/register', 'GuestController@formRegister')->name('form_register')->middleware('guest');
Route::get('/', 'GuestController@index')->name('first_page');

// check age
Route::post('/check_age/{id}', 'GuestController@checkAge')->name('check_age_post');
Route::get('/check_age/{id}', 'GuestController@checkAge')->name('check_age');
// detail game page
Route::get('/detail_game/{id}', 'GuestController@detailGame')->name('detail_game');
// shopping cart
Route::delete('/shopping_chart/delete/{id}', 'TransactionController@destroy')->name('shopping_chart_page')->middleware('member');
Route::post('/shopping_chart/buy/{id}', 'TransactionController@shoppingChart')->name('shopping_chart_page')->middleware('member');
Route::get('/shopping_chart', 'TransactionController@chart')->name('shopping_chart_page')->middleware('member');
// shopping infotmation
// Route::post('/shopping_information', 'TransactionController@shoppingInformation')->name('update_shopping_information_page');
Route::put('/shopping_information/update', 'TransactionController@updateShoppingInformation')->name('update_information_page')->middleware('member');;
Route::get('/shopping_information', 'TransactionController@shoppingInformation')->name('shopping_information_page')->middleware('member');;
// shopping receipt
Route::get('/shopping/transaction_receipt', 'TransactionController@show')->name('transaction_receipt_page')->middleware('member');;


// profile member
Route::put('/member_profile/update', 'GuestController@updateMemberProfile')->name('update_member_profile_page')->middleware('member');
Route::get('/member_profile', 'GuestController@memberProfile')->name('member_profile_page')->middleware('member');
// member friend
Route::delete('/member_friend/reject/{id}', 'FriendController@reject')->name('reject_friend_request')->middleware('member');
Route::delete('/member_friend/cancel/{id}', 'FriendController@cancel')->name('cancel_friend_request')->middleware('member');
Route::put('/member_friend/update/{id}', 'FriendController@update')->name('acc_friend')->middleware('member');
Route::post('/member_friend', 'FriendController@addFriend')->name('add_friend')->middleware('member');
Route::get('/member_friend', 'FriendController@index')->name('member_friend')->middleware('member');
// member transaction history
Route::get('/member_transaction_history', 'TransactionController@memberHistory')->name('member_transaction_history_page');
